# LineageOS 14.1 - Moto G (Falcon) Archive

This repository contains the latest 2 builds officially released by LineageOS team before the device being deprecated.

The install instructions and device info was archive with [Internet Archive](https://web.archive.org)

## Links and Instructions

- Info - https://web.archive.org/web/20190222023550/https://wiki.lineageos.org/devices/falcon
- Install - https://web.archive.org/web/20190222023550/https://wiki.lineageos.org/devices/falcon/install
- Build - https://web.archive.org/web/20190225154531/https://wiki.lineageos.org/devices/falcon/build

## Downloads

You can check the integrity using the archived hashes. Files are stored on [MEGA](https://mega.nz) and were also archived on Internet Archive.

- `lineage-14.1-20190207-nightly-falcon-signed.zip` - [[Internet Archive](https://web.archive.org/web/20190221230455/https://mirrorbits.lineageos.org/full/falcon/20190207/lineage-14.1-20190207-nightly-falcon-signed.zip)] [[MEGA](https://mega.nz/#!fEYHSS5Z!Inbg97xNpWfP12BdMlWFsPgx6TRuG_uqbx27fFcYvwo)]

    Size: 306.0 MB  
    Build date: 2019-02-07  
    Integrity [sha256]: https://web.archive.org/web/20190221230455/https://mirrorbits.lineageos.org/full/falcon/20190207/lineage-14.1-20190207-nightly-falcon-signed.zip?sha256


- `lineage-14.1-20190107-nightly-falcon-signed.zip` - [[Internet Archive](https://web.archive.org/web/20190221230455/https://mirrorbits.lineageos.org/full/falcon/20190107/lineage-14.1-20190107-nightly-falcon-signed.zip)] [[MEGA](https://mega.nz/#!3BRjXSpB!SrQvjaFwaAyjTv_I9-UcA0yubIa-n_eLDk_NKCCHFTA)]  

    Size: 305.73 MB  
    Build date: 2019-01-07  
    Integrity [sha256]: https://web.archive.org/web/20190221230455/https://mirrorbits.lineageos.org/full/falcon/20190107/lineage-14.1-20190107-nightly-falcon-signed.zip?sha256
